# Evision FrontEnd Test

#### Demo Link on AWS S3: [Click Here for Live Demo](http://evision-frontend.s3-website.eu-central-1.amazonaws.com/)

## Deployment
* I have deployed the app to AWS S3 for demo purposes.
* I can also use Heroku but mostly Amazon web services.

## Project Instructions
* Clone the repository.
* Run `npm install`
* For starting the app run `npm start`
* Server runs at the same time with react app after `npm start`.
* Open [http://localhost:3000](http://localhost:3000) to view the app in the browser.
#### CORS
* App `put request` works great on every browser except Firefox.
* There is a CORS problem happening and `put request` not working properly on Firefox because of the server and app running on localhost but on different ports.
* If I have permission to change server.js, i could handle that problem. On frontend side there was nothing to do, I have tried so many solutions like adding headers while put request triggering. At the other hand, we won't get any CORS error, if the API would be on Heroku or etc.

## Used External Libraries
* Axios (used for asyncronous fetching)

## Changes on Server
* Nothing changed on server.

## How did I handle the random 503 error?
* First, I have investigated the reasons of 503 error on server and find out there is a Math.random() generating a random value. If the value equal or greater than 0.8, it returns 503 error.
* So, I have catched the response error status with `catch` and if it's 503, I set the condition to reload the DOM with `reload()` method.

## Features
* Created as responsive.
* Account details fetched from server with Axios.
* If any credit/debit added, total balance will increase/change. Total balance connected to new credit/debit which will be added later.
* If the input fields are empty, it returns a warning alert and not put request.
* You can choose the credit/debit type as `to` | `from`.
* After adding a credit/debit with submit, the credit/debit will goes to table as new row.
* Created a filter toggle for filtering the table elements as `from / to`. You can try it with clicking the orangish `From / To` heading element.
* Date automatically generates and included into put request when a credit/debit addition triggered.
* Table section is scrollable when the value reaches `max-height`.

## What I have learned from demo project?
* I had some problems with `catching errors`. But the project improved my knowledge.
* I experienced a problematic server with random errors. (like in the real life)
* I can not wait to see how good the learning opportunites in Evision. Because, demo app is reflected that idea.
* Thanks to Evision, I also signed up for a Bitbucket account over Github.

## How can the app be improved?
* With unit tests. I'm not good at but trying to learn these days.
* With more detailed device css.
* Server-side rendering practices like `react snap` library or building the entire project onto Next.js.
* There was not needed but if its, you can use Redux for state management. I can not, still learning.

## Need Practice and Still Learning
* Redux
* Dockerizing

## What are the other technologies i can use if needed?
* Styled Components
* Material UI
* Semantic UI
* SSH (Learned on DigitalOcean and Raspberry Pi's owned by me then evolved to using git etc.)
* Custom boilerplate. (not only create-react-app)
* Wordpress
* PHP (but a few)
* Postgresql
* Mysql / PHPMyadmin
* WHM - Managing the C-Panels
* By the motivation support from Evision, I can add any tech skill to my skillset when it's needed.