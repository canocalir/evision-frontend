import React from 'react';
import AccountApp from '../AccountApp/AccountApp';
import './Main.css';

function Main() {
  return (
    <div className="App">
      <AccountApp/>
    </div>
  );
}

export default Main;
