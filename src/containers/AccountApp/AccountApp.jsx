import React, { Component } from 'react';
import Details from '../../sections/Details/Details';
import Updater from '../../sections/Updater/Updater';
import './AccountApp.scss';
import ScrollTable from '../../sections/ScrollTable/ScrollTable';
import Axios from 'axios';



export default class AccountApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bankaccounts: [],
            alldata: [],
            debandcredits: []
        }
    }

    async componentDidMount() {
        await Axios.get('http://localhost:8080/api/balance')
        .then((response) => {
        const alldata = response.data
        this.setState({bankaccounts: alldata.account})
        this.setState({alldata: alldata})
        this.setState({debandcredits: alldata.debitsAndCredits})  
        })
        .catch((error) => {
            if(error.response.status === 503) {
                window.location.reload();
            }
        })   
    }

    sumValueHandler = () => {
        let defaultBalance = this.state.bankaccounts.balance;
        let fromor = this.state.debandcredits.filter((from)=> {
            return (from.from)
        });
        let tomor = this.state.debandcredits.filter((to) => {
            return (to.to)
        });

        let toBalance = tomor.map(obj => obj.amount);
        let fromBalance = fromor.map(obj => obj.amount);

        toBalance.reduce((acc,val) => {
            return acc + val
        },0)
        fromBalance.reduce((acc,val) => {
            return acc + val
        },0)
        return parseInt(defaultBalance);
    }

    render() { 
        
        const {bankaccounts,alldata,debandcredits} = this.state;

        return (
            <div className="appcontainer">
           <Details 
            totalbalance={this.sumValueHandler()}
            name={bankaccounts.name} iban={bankaccounts.iban} 
            currency={alldata.currency}/>
           <Updater/>
           <ScrollTable 
           datasend={debandcredits}/>
        </div>
        )
    }
}
