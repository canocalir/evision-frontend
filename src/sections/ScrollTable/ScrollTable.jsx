import React, { Component } from 'react'
import './ScrollTable.scss';



export default class ScrollTable extends Component {
	constructor(props){
		super(props);
		this.state = {
			isFrom: false,
			fromVal: [],
			buttoncolor: "",
			buttoncolor2: ""
		}
	}

	onClickHandler = () => {
		this.setState({isFrom: !this.state.isFrom})
		this.setState({fromVal: this.props.datasend.filter((fromval)=>{
			if(this.state.isFrom) {
				return fromval.from
			} else {
				return fromval.to
			}
		})
	})
			if(this.state.isFrom) {
		this.setState({buttoncolor: "#fb8d17", buttoncolor2: ""})
			} else {
				this.setState({buttoncolor:"", buttoncolor2: "#fb8d17"})
			}
}

	componentDidMount() {
		window.addEventListener('load',this.onClickHandler)
	}

	

	render() {
		
	let content;
		content = this.state.fromVal.map(( listValue, index ) => {
			return (
				  <tr key={index}>
					<td>{listValue.from||listValue.to}</td>
					<td>{listValue.description}</td>
					<td>{listValue.date}</td>
					<td className="amount-text">
						{listValue.amount}
						</td>
				  </tr>
				);
			  })
		
		return (

			<div className="scrollcontainer">
            <div className="scrollbox">
            <table>
						<thead>
						<tr>
						<th 
						style={{cursor:"pointer"}} 
						onClick={(e) => {this.onClickHandler(e)}}>
						
						<span 
						style={{color: this.state.buttoncolor}}>
						From </span>
						/ <span style={{color: this.state.buttoncolor2}}>To</span></th>
						<th>Description</th>
						<th>Date</th>
        		<th>Amount</th>
					 </tr>
					</thead>
					<tbody>
					{content}
					</tbody>
					</table>
            </div>
        </div>
    )
}
}
