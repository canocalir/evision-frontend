import React, { Component } from 'react';
import './Updater.scss';
import Axios from 'axios';


export default class Updater extends Component {
    constructor() {
      super();
      this.state = {
        amount: 0,
        description: "",
        from: true,
        to: false,
        date: new Date()
      };
    }
  
    onAmountChange = e => {
      this.setState({
        amount: parseInt(e.target.value)
      });
    };

    onDescriptionChange = e => {
      this.setState({
        description: e.target.value
      });
    };

    onSelectTypeChange = e => {
      this.setState({
        [e.target.value === "from" ? "from" : "to"]: true,
        [e.target.value !== "from" ? "from" : "to"]: false
      });
      if(e.target.value !== "from" && (this.state.from != null || this.state.from !== "")) { 
        this.setState({
          to: this.state.from,
          from: null
        });
      } else if(e.target.value === "from" && (this.state.to != null || this.state.to !== "")){
        this.setState({
          from: this.state.to,
          to: null
        });
      }
    };

    onFromToChange = (e) => {
      this.setState({
         [this.state.from ? "from" : "to"]: e.target.value
      });
    }
    onSubmitEdit = () => {
      window.location.reload();

      if(this.state.amount && this.state.description !== null){
      Axios.put(
        "http://localhost:8080/api/balance/add",
        {
          ...this.state,  
        },
        { headers: 
            { "Content-Type": "application/json",
             "Access-Control-Allow-Origin": "*",
             "Access-Control-Allow-Methods": "OPTIONS, GET, PUT" } }
      )
        .then(response => {
          // handle Response
          console.log("RESPONSE RECEIVED: ", response);
        })
        .catch(err => {
          // handle Error
          console.log("AXIOS ERROR: ", err);
        });
      } else {
        alert("Fields must be filled")
      }
    };
  
    render() {
      return (
        <div className="updatercontainer">
          <div className="updaterinputs">
            <input
              required
              onChange={this.onAmountChange}
              className="amount"
              name="amount"
              type="number"
              placeholder="Enter Amount"
            />
            <input
              required
              onChange={this.onDescriptionChange}
              className="description"
              name="description"
              type="text"
              placeholder="Enter Description"
            />
          </div>
          <div className="selectbox">
            <select onChange={this.onSelectTypeChange}>
              <option value="from">From</option>
              <option value="to">To</option>
            </select>
            <input 
            required
            onChange={this.onFromToChange} 
            className="fromto" 
            type="text"
            name="fromto" 
            placeholder="Enter From or To Name"/>
          </div>
          <div className="selectboxcontainer">
            <div onClick={this.onSubmitEdit} 
            className="button-container">
              <a href="#" className="button amount-submit">
                <span>Update</span>
              </a>
            </div>
          </div>
        </div>
      );
    }
  }