import React from 'react';
import './Details.scss';
import Logo from './logo.png';

export default function Details(props) {
    return (
        <div className="detailscontainer">
        <div className="logoandtext">
        <img src={Logo} alt="bank-logo"/>
        <p>You can send and receive money with our app.</p>
        </div>
            <div className="detailsinline">
            <h1>Account Details</h1>
            <p><span className="s1">Name: </span>{props.name}</p>
            <p><span className="s2">IBAN: </span>{props.iban}</p>
            <p><span className="s3">Balance:  </span><span style={{color:"#fb8d17", fontSize:"24px"}}>€ {props.totalbalance}</span></p>
            <p><span className="s4">Currency: </span>{props.currency}</p>
            </div>
        </div>
    )
}
